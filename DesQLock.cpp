/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "Global.hpp"

#include <desq/desq-config.h>
#include <desq/Utils.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/InputInhibition.hpp>

#include "DesQLock.hpp"

// Helper functions
#include "DesQLockUtils.cpp"

struct pam_response *reply;

DesQ::Lock::UI::UI() : QWidget() {
    /* User Icon */
    QLabel *userIcon = new QLabel();

    userIcon->setFixedSize( QSize( 72, 72 ) );
    userIcon->setPixmap( getUserIcon().pixmap( QSize( 72, 72 ) ) );
    userIcon->setObjectName( "UserIcon" );

    QHBoxLayout *ilyt = new QHBoxLayout();

    ilyt->setContentsMargins( QMargins() );
    ilyt->setSpacing( 0 );
    ilyt->addStretch();
    ilyt->addWidget( userIcon );
    ilyt->addStretch();

    /* User Name */
    QStringList nameInfo  = getUserInfo();
    QLabel      *userName = new QLabel( QString( "%1 (%2)" ).arg( nameInfo[ 0 ] ).arg( nameInfo[ 1 ] ) );

    userName->setFixedHeight( 36 );
    userName->setAlignment( Qt::AlignCenter );
    userName->setObjectName( "UserName" );

    QHBoxLayout *nlyt = new QHBoxLayout();

    nlyt->setContentsMargins( QMargins() );
    nlyt->setSpacing( 0 );
    nlyt->addStretch();
    nlyt->addWidget( userName );
    nlyt->addStretch();

    passwdLE = new QLineEdit( this );
    passwdLE->setFixedSize( 270, 36 );
    passwdLE->setPlaceholderText( "Password" );
    passwdLE->setEchoMode( QLineEdit::Password );
    passwdLE->setAlignment( Qt::AlignCenter );
    passwdLE->setObjectName( "PasswdLE" );
    connect( passwdLE, &QLineEdit::returnPressed, this, &DesQ::Lock::UI::authenticate );

    unlockBtn = new QToolButton( this );
    unlockBtn->setFixedSize( 36, 36 );
    unlockBtn->setIconSize( QSize( 32, 32 ) );
    unlockBtn->setIcon( QIcon::fromTheme( "arrow-right" ) );
    unlockBtn->setObjectName( "UnlockBtn" );
    connect( unlockBtn, &QToolButton::clicked, this, &DesQ::Lock::UI::authenticate );

    QHBoxLayout *plyt = new QHBoxLayout();

    plyt->setContentsMargins( QMargins() );
    plyt->setSpacing( 0 );
    plyt->addStretch();
    plyt->addWidget( passwdLE );
    plyt->addWidget( unlockBtn );
    plyt->addStretch();

    QVBoxLayout *lyt = new QVBoxLayout();

    lyt->addStretch();
    lyt->addLayout( ilyt );
    lyt->addLayout( nlyt );
    lyt->addWidget( new QLabel( "  " ) );
    lyt->addLayout( plyt );
    lyt->addStretch();

    setStyleSheet( qss );

    setLayout( lyt );
    bgImage = getBackgroundImage();

    setWindowTitle( "DesQ ScreenLock" );

    setFixedSize( qApp->primaryScreen()->size() );
    setWindowFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::BypassWindowManagerHint );
    setAttribute( Qt::WA_X11NetWmWindowTypeDock );
}


DesQ::Lock::UI::~UI() {
    delete passwdLE;
    delete unlockBtn;
}


void DesQ::Lock::UI::authenticate() {
    disable();

    QString username = qgetenv( "USER" );
    QString password = passwdLE->text();

    const struct pam_conv local_conversation = { function_conversation, NULL };
    pam_handle_t          *local_auth_handle = NULL;

    int retval;
    retval = pam_start( "su", username.toUtf8().data(), &local_conversation, &local_auth_handle );

    /* Auth init failure: Should we crash out? */
    if ( retval != PAM_SUCCESS ) {
        restore();

        return;
    }

    reply = ( struct pam_response * )malloc( sizeof(struct pam_response) );

    reply[ 0 ].resp         = strdup( password.toUtf8().data() );
    reply[ 0 ].resp_retcode = 0;
    retval = pam_authenticate( local_auth_handle, 0 );

    /* Auth failure */
    if ( retval != PAM_SUCCESS ) {
        restore();
        return;
    }

    retval = pam_end( local_auth_handle, retval );

    /* All was successful. Close the app to unlock the screen */
    emit authenticated();
}


void DesQ::Lock::UI::disable() {
    passwdLE->setStyleSheet( leDisabled );
    unlockBtn->setStyleSheet( tbDisabled );
    qApp->processEvents();

    passwdLE->setDisabled( true );
    unlockBtn->setDisabled( true );
    qApp->processEvents();
}


void DesQ::Lock::UI::restore() {
    passwdLE->setEnabled( true );
    unlockBtn->setEnabled( true );

    passwdLE->setStyleSheet( leNormal );
    unlockBtn->setStyleSheet( tbNormal );
    qApp->processEvents();

    passwdLE->selectAll();
    passwdLE->setFocus();
}


void DesQ::Lock::UI::show() {
    qputenv( "QT_WAYLAND_SHELL_INTEGRATION", "DesQLock" );
    QWidget::show();

    /** This is exclusively for wayfire - requires wayfire dbusqt plugin */
    if ( WQt::Utils::isWayland() ) {
        WQt::LayerShell::LayerType lyr  = WQt::LayerShell::Overlay;
        WQt::LayerSurface          *cls = wlRegistry->layerShell()->getLayerSurface( windowHandle(), nullptr, lyr, "lockscreen" );

        cls->setAnchors(
            WQt::LayerSurface::Top |
            WQt::LayerSurface::Right |
            WQt::LayerSurface::Bottom |
            WQt::LayerSurface::Left
        );

        /** Size of our surface */
        cls->setSurfaceSize( size() );

        /** No gap anywhere */
        cls->setMargins( QMargins( 0, 0, 0, 0 ) );

        /** We may need keyboard interaction (in future). Nothing needed now */
        cls->setKeyboardInteractivity( WQt::LayerSurface::Exclusive );

        /** Commit to our choices */
        cls->apply();
    }

    qunsetenv( "QT_WAYLAND_SHELL_INTEGRATION" );

    QEventLoop *execLoop = new QEventLoop( this );
    connect(
        this, &DesQ::Lock::UI::authenticated, [ = ]() {
            close();
            deleteLater();

            qApp->processEvents();
            execLoop->quit();
        }
    );

    passwdLE->setFocus();

    execLoop->exec();
}


void DesQ::Lock::UI::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.drawImage( geometry(), bgImage );
    painter.end();

    QWidget::paintEvent( pEvent );

    pEvent->accept();
}
