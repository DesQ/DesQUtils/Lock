/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <signal.h>

// Local Headers
#include "Global.hpp"
#include "Manager.hpp"
#include "DesQLock.hpp"

#include <desq/Utils.hpp>
#include <desq/desq-config.h>

#include <DFApplication.hpp>
#include <DFXdg.hpp>
#include <DFUtils.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/InputInhibition.hpp>
#include <wayqt/SessionLock.hpp>

DFL::Settings           *lockSett   = nullptr;
WQt::Registry           *wlRegistry = nullptr;
WQt::InputInhibitor     *inhibitor  = nullptr;
WQt::SessionLockManager *lockMgr    = nullptr;

int main( int argc, char **argv ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Lock.log" ).toLocal8Bit().data(), "a" );

    qInstallMessageHandler( DFL::Logger );

    QByteArray date = QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8();

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Lock started at" << date.constData();
    qDebug() << "------------------------------------------------------------------------\n";

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );

    DFL::Application app( argc, argv );

    app.setApplicationName( "Lock" );
    app.setOrganizationName( "DesQ" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desk-lock" );

    /** Remain open */
    app.setQuitOnLastWindowClosed( false );

    app.interceptSignal( SIGSEGV, true );

    QCommandLineParser parser;

    parser.addHelpOption();         // Help
    parser.addVersionOption();      // Version

    /* Path to the background file */
    parser.addOption( { { "b", "background" }, "The path to the background file.", "image file" } );

    /* Path to the background file */
    parser.addOption( { { "o", "output" }, "Output on which the background is to be changed", "output" } );

    /* Save this? */
    parser.addOption( { { "s", "save" }, "Save the image file in settings for the next start." } );

    /* Process the CLI args */
    parser.process( app );

    if ( app.lockApplication() ) {
        lockSett   = DesQ::Utils::initializeDesQSettings( "Lock", "Lock" );
        wlRegistry = new WQt::Registry( WQt::Wayland::display() );

        QString             bgValue( parser.value( "background" ) );
        DesQ::Lock::Manager *screenLockMgr = new DesQ::Lock::Manager( bgValue );

        /** Automatically create the WQt::SessionLock object when this interface becomes available. */
        QObject::connect(
            wlRegistry, &WQt::Registry::interfaceRegistered, [ = ] ( WQt::Registry::Interface iface ) {
                if ( iface == WQt::Registry::SessionLockManagerInterface ) {
                    lockMgr = wlRegistry->sessionLockManager();
                    screenLockMgr->acquireLock();
                }
            }
        );

        wlRegistry->setup();

        QObject::connect(
            qApp, &DFL::Application::screenAdded, [ = ] ( QScreen *scrn ) {
                lockSett->mapDynamicKey( "Background",         "Background:" + scrn->name() );
                lockSett->mapDynamicKey( "BackgroundPosition", "BackgroundPosition:" + scrn->name() );
                screenLockMgr->createInstance( scrn, QString() );
            }
        );

        QObject::connect( &app, &DFL::Application::messageFromClient, screenLockMgr, &DesQ::Lock::Manager::handleMessages );

        // WQt::InputInhibitManager *inhibitMgr = wlRegistry->inputInhibitManager();
        // inhibitor = inhibitMgr->getInputInhibitor();
        //
        // lockSett = new DFL::Settings( "DesQ", "Lock", ConfigPath );
        //
        // DesQLock *lock = new DesQLock();
        // lock->show();
    }

    else {
        if ( parser.isSet( "background" ) ) {
            QString msg( "background\n" );

            /** Save this permanently */
            if ( parser.isSet( "save" ) ) {
                msg = "set-" + msg;
            }

            /** Use this background only on a particular output */
            if ( parser.isSet( "output" ) ) {
                msg += parser.value( "output" ) + "\n";
            }

            /** Name of the background file */
            msg += parser.value( "background" );
            app.messageServer( msg );
        }

        else if ( parser.isSet( "save" ) and not parser.isSet( "background" ) ) {
            qDebug() << "-s/--save cannot be used alone.";
            parser.showHelp();
        }

        else if ( parser.isSet( "output" ) and not parser.isSet( "background" ) ) {
            qDebug() << "-o/--output cannot be used alone.";
            parser.showHelp();
        }

        else {
            qFatal( "Another instance of DesQ Lock is already active. Aborting..." );
            return 1;
        }

        return 0;
    }

    return 0;
}
