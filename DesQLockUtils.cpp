/*
 *
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include "Global.hpp"
#include <security/pam_appl.h>

#include <pwd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

int function_conversation( int, const struct pam_message **, struct pam_response **resp, void * ) {
    *resp = reply;
    return PAM_SUCCESS;
}


QIcon getUserIcon() {
    QString usrIconStr;

    if ( DesQ::Utils::isReadable( QDir::home().filePath( ".face.icon" ) ) ) {
        usrIconStr = QDir::home().filePath( ".face.icon" );
    }

    else if ( DesQ::Utils::isReadable( QDir::home().filePath( ".face" ) ) ) {
        usrIconStr = QDir::home().filePath( ".face" );
    }

    else {
        usrIconStr = "";
    }

    if ( usrIconStr.isEmpty() ) {
        return QIcon::fromTheme( "user" );
    }

    else {
        return QIcon( usrIconStr );
    }
}


QStringList getUserInfo() {
    QString       rName, uName;
    struct passwd pwd;
    struct passwd *result;
    char          buf[ 16484 ] = { 0 };
    int           ret          = getpwuid_r( getuid(), &pwd, buf, 16384, &result );

    if ( ret ) {            // There was some error - should not have happened
        rName = DesQ::Utils::baseName( QDir::homePath() );
        uName = rName;
    }

    else {                      // There was no error
        if ( result == NULL ) { // Record was not found
            rName = DesQ::Utils::baseName( QDir::homePath() );
            uName = rName;
        }
        else {
            rName = QString( pwd.pw_gecos ).split( "," ).takeFirst();
            uName = pwd.pw_name;
        }
    }

    return QStringList( { rName, uName } );
}


QImage getBackgroundImage() {
    QString imageStr = lockSett->value( "Background" );
    QString imageClr = QString( "#%1" ).arg( ( QString )lockSett->value( "BGColor" ) );

    if ( imageStr == "default" ) {
        imageStr = QDir( SharePath ).filePath( "resources/screenlock.jpg" );
    }

    else if ( imageStr == "none" ) {
        imageStr = "";
    }

    else if ( not DesQ::Utils::isReadable( imageStr ) ) {
        imageStr = SharePath + QString( "resources/screenlock.jpg" );
    }

    QImage bgImage;

    if ( imageStr.isEmpty() ) {
        bgImage = QImage( qApp->primaryScreen()->size(), QImage::Format_ARGB32 );
        bgImage.fill( imageClr );
    }

    else {
        bgImage = QImage( imageStr ).scaled( qApp->primaryScreen()->size(), Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation );
    }

    return bgImage;
}


QString qss = QString(
    "#UserIcon{"
    "border: 2px solid palette(Window);"
    "border-radius: 5px;"
    "}"
    "#UserName{"
    "border: 1px solid gray;"
    "background: rgba(180, 180, 180, 180);"
    "border-radius: 18px; color: black;"
    "padding-left: 10px;"
    "padding-right: 10px;"
    "font-size: 150%;"
    "}"
    "#PasswdLE{"
    "background: white;"
    "border: 1px solid gray;"
    "border-right: 0px;"
    "color: black;"
    "}"
    "#UnlockBtn{"
    "background: white;"
    "border: 1px solid gray;"
    "border-left: 0px;"
    "}"
);

QString leNormal = QString(
    "QLineEdit{"
    "background: white;"
    "border: 1px solid gray;"
    "border-right: 0px;"
    "color: black;"
    "}"
);

QString leDisabled = QString(
    "QLineEdit{"
    "background: gray;"
    "border: 1px solid gray;"
    "border-right: 0px;"
    "color: black;"
    "}"
);

QString tbNormal = QString(
    "#UnlockBtn{"
    "background: white;"
    "border: 1px solid gray;"
    "border-left: 0px;"
    "}"
);

QString tbDisabled = QString(
    "#UnlockBtn{"
    "background: gray;"
    "border: 1px solid gray;"
    "border-left: 0px;"
    "}"
);
