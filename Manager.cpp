/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "DesQLock.hpp"
#include "Manager.hpp"
#include "Global.hpp"

#include <QSequentialAnimationGroup>
#include <QVariantAnimation>
#include <QEasingCurve>
#include <QScreen>

#include <DFApplication.hpp>

DesQ::Lock::Manager::Manager( QString imgLoc ) {
    bgImg = imgLoc;

    for ( QScreen *scrn: qApp->screens() ) {
        lockSett->mapDynamicKey( "Background", "Background:" + scrn->name() );
        createInstance( scrn, imgLoc );
    }

    connect( lockSett, &DFL::Settings::settingChanged, this, &DesQ::Lock::Manager::reloadSettings );

    /** Delete the instance and the layer surface when the screen is disconnected */
    connect(
        qApp, &DFL::Application::screenRemoved, [ = ] ( QScreen *screen ) {
            if ( mInstances.contains( screen->name() ) ) {
                WQt::SessionLockSurface *surf = mSurfaces.take( screen->name() );
                delete surf;

                DesQ::Lock::UI *ui = mInstances.take( screen->name() );
                ui->close();
                delete ui;
            }
        }
    );
}


DesQ::Lock::Manager::~Manager() {
    for ( WQt::SessionLockSurface *surf: mSurfaces.values() ) {
        delete surf;
    }
    mSurfaces.clear();

    for ( DesQ::Lock::UI *ui: mInstances.values() ) {
        ui->close();
        delete ui;
    }
    mInstances.clear();
}


bool DesQ::Lock::Manager::acquireLock() {
    mLock = lockMgr->lock();

    connect( mLock, &WQt::SessionLock::lockAcquired, this, &DesQ::Lock::Manager::prepareLockSurfaces );
    connect( mLock, &WQt::SessionLock::lockFinished, this, &DesQ::Lock::Manager::prepareToUnlock );

    /** Nothing to do here */
    // connect ( mLock, &WQt::SessionLock::lockFailed, [=] () {} );
    return true;
}


void DesQ::Lock::Manager::handleMessages( QString msg, int fd ) {
    if ( msg.startsWith( "background\n" ) ) {
        QStringList parts = msg.split( "\n" );

        /**
         * Output name is specified.
         */
        if ( parts.count() == 3 ) {
            if ( mInstances.contains( parts[ 1 ] ) ) {
                mInstances[ parts[ 1 ] ]->update( parts[ 2 ] );
                qApp->messageClient( "done", fd );

                return;
            }

            else {
                qWarning() << parts[ 1 ] << "is not a valid output";
            }

            qApp->messageClient( "failed", fd );
        }

        else {
            for ( DesQ::Lock::UI *ui: mInstances.values() ) {
                ui->update( parts[ 1 ] );
            }

            qApp->messageClient( "done", fd );
        }
    }

    else if ( msg.startsWith( "set-background" ) ) {
        QStringList parts = msg.split( "\n" );

        if ( parts.length() == 3 ) {
            lockSett->setValue( "Background:" + parts[ 1 ], parts[ 2 ] );

            if ( mInstances.contains( parts[ 1 ] ) ) {
                mInstances[ parts[ 1 ] ]->update( parts[ 2 ] );
                qApp->messageClient( "done", fd );

                return;
            }

            qApp->messageClient( "failed", fd );
        }

        else {
            lockSett->setValue( "Background", parts[ 1 ] );
            for ( DesQ::Lock::UI *ui: mInstances.values() ) {
                ui->update( parts[ 1 ] );
            }

            qApp->messageClient( "done", fd );
        }
    }

    else {
        qWarning() << "Unhandled request:" << msg;
        qApp->messageClient( "Unhandled request", fd );
    }
}


void DesQ::Lock::Manager::createInstance( QScreen *scrn, QString bgImg ) {
    DesQ::Lock::UI *lock = new DesQ::Lock::UI();

    lock->update( bgImg );

    lock->show();

    if ( WQt::Utils::isWayland() ) {
        /** wl_output corresponding to @screen */
        wl_output *output = WQt::Utils::wlOutputFromQScreen( scrn );

        WQt::SessionLockSurface *surf = mLock->getLockSurface( lock->windowHandle(), output );

        /** Resize the instance and the layer surface when screen size changes */
        connect(
            surf, &WQt::SessionLockSurface::resizeLockSurface, [ = ]( const QSize newSize ) {
                lock->resize( newSize );
            }
        );

        surf->setup();

        mInstances[ scrn->name() ] = lock;
        mSurfaces[ scrn->name() ]  = surf;
    }

    else {
        lock->close();
        delete lock;
    }
}


void DesQ::Lock::Manager::reloadSettings( QString key, QVariant ) {
    /** General */
    if ( (key == "Background") or (key == "BackgroundPosition") ) {
        for ( DesQ::Lock::UI *ui: mInstances.values() ) {
            ui->update( key );
            ui->update( "opacity" );
        }
    }

    /** Output specific */
    else if ( key.startsWith( "Background:" ) or key.startsWith( "BackgroundPosition:" ) ) {
        QStringList parts  = key.split( ":" );
        QString     output = parts[ 1 ];

        if ( mInstances.contains( output ) ) {
            mInstances[ output ]->update( key );
            mInstances[ output ]->update( "opacity" );
        }
    }
}


void DesQ::Lock::Manager::prepareLockSurfaces() {
    for ( QScreen *scrn: qApp->screens() ) {
        createInstance( scrn, bgImg );
    }
}


void DesQ::Lock::Manager::prepareToUnlock() {
    for ( QString scrn: mInstances.keys() ) {
        WQt::SessionLockSurface *surf = mSurfaces.take( scrn );
        delete surf;

        DesQ::Lock::UI *inst = mInstances.take( scrn );
        inst->close();
        delete inst;
    }

    mLock->unlockAndDestroy();
}
