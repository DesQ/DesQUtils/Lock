/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtCore>

#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/WayQtUtils.hpp>

#include <wayqt/SessionLock.hpp>

namespace DesQ {
    namespace Lock {
        class Manager;
        class UI;
    }
}

class QVariantAnimation;

/**
 * @class DesQ::Lock::Manager
 * This class will handle the creation of DesQ::Lock::UI instances.
 * Additionally, it will also handle the CLI requests, to change and
 * save wallpapers.
 */
class DesQ::Lock::Manager : public QObject {
    Q_OBJECT;

    public:
        Manager( QString );
        ~Manager();

        /**
         * Acquire lock.
         */
        bool acquireLock();

        /**
         * Handle the requests from other instance. To reply to queries,
         * write suitable data to the file descriptor @fd.
         */
        Q_SLOT void handleMessages( QString msg, int fd );

        /**
         * Start an instance of DesQ::Lock::UI on screen @scrn
         */
        void createInstance( QScreen *scrn, QString );

    private:

        /**
         * This is the bgImage specified in the CLI or set temporarily by
         * another instance. Will be used to start new instances.
         */
        QString bgImg;

        /**
         * This is the lock acquired from WQt::SessionLockManager.
         * This lock will be used to set lock surfaces on various outputs.
         */
        WQt::SessionLock *mLock;

        /**
         * We need to remember all the instances to change the bg image
         * on one or all of them.
         */
        QHash<QString, DesQ::Lock::UI *> mInstances;

        /**
         * We need to remember all the WQt::SessionLockSurface to change the
         * size of the surfaces when screen size changes.
         */
        QHash<QString, WQt::SessionLockSurface *> mSurfaces;

        /**
         * Reflect the externally made changes.
         * Some setting was changed externally; propagate that change.
         */
        void reloadSettings( QString, QVariant );

        /** We've acquired the lock, and are now ready to commit the lock surfaces. */
        void prepareLockSurfaces();

        /** The lock is to be released. Prepare to close the surfaces */
        void prepareToUnlock();
};
