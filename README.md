# DesQLock
## A screen locker for DesQ DE

A very simple screen locker built for use with DesQ and other wlroots compositors.

<img src = 'screenshot.jpg' width = "800">

## Dependencies
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* Qt
* libdesq (https://gitlab.com/DesQ/libdesq)
* wlrootsqt (https://gitlab.com/desktop-frameworks/wayqt)
* libpam (libpam0g-dev)


## Compile and install

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQUtils/Lock.git DesQLock`
- Enter the `DesQLock` folder
  * `cd DesQLock`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


## Usage
You can do one or more of the following
- Just fire-up a terminal and type `exec <prefix>/<libdir>/libexec/desq/desq-lock`
- Add the following two lines to your wayfire configuration file under `[commands]` section to start it with a keybinding
    ```ini
    binding_screenlock = <super> <shift> KEY_L
    command_screenlock = <prefix>/<libdir>/libexec/desq/desq-lock
    ```
- To automatically lock screen using swayidle, modify your wayfire configuration as follows:
    ```ini
    [autostart]
    ...
    ...
    idle = swayidle before-sleep <prefix>/<libdir>/libexec/desq/desq-lock
    ...
    ```

### Configuration

There are two configuration options: `Background` and `BGColor`
- We first check if `Background` set. It can have three values: `default` (loads the inbuilt image as the bg), `none` (No background image), or
absolute path to image file. If the image file is not found, then default image will be loaded.
- If the `Background` is set to `none`, then, we paint the background with a solid color specified with `BGColor`. It takes RGB hex values as
single continuous string. Ex: FFFFFF (white), 008080 (teal) etc. Default: CE9F6F

## TODO
- [**Multi-screen support**](https://gitlab.com/marcusbritanicus/wf-lock/-/issues/1)
- NumLock and CapsLock indicators
- A good CLI

## Knwon issues
- If any other surface has already requested input-inhinitor, the lockscreen will not load.
